<?php

/**
 * @file
 * Main code for the Druleniun integration in Aegir.
 */

/**
 * Implements hook_permission().
 */
function hosting_drulenium_permission() {
  return array(
    'create dr_create task' => array(
      'title' => t('Drulenium run Screenshot task'),
      'description' => t('Start Drulenium task to take screenshots'),
    ),
    'create dr_run_compare task' => array(
      'title' => t('Drulenium run Compare task'),
      'description' => t('Start Drulenium task to run compare'),
    ),
  );
}

/**
 * Implements hook_hosting_tasks().
 */
function hosting_drulenium_hosting_tasks() {
  $tasks = array();

  $tasks['site']['dr_create'] = array(
    'title' => t('Drulenium - Screenshots'),
    'description' => t('Create screenshots.'),
  );
  $tasks['site']['dr_run_compare'] = array(
    'title' => t('Drulenium - Compare'),
    'description' => t('Create screenshots & Diff to reference site.'),
    'dialog' => TRUE,
  );

  return $tasks;
}

/**
 * Implements hosting_task_TASK_TYPE_form().
 */
function hosting_task_dr_run_compare_form($node) {

  $value = t("This will compare the current site @name with the reference specified below.", array('@name' => $node->title));
  $form['operations'] = array(
    '#type' => 'item',
    '#markup' => $value,
    '#title' => '',
  );

  $form['reference'] = array(
    '#type' => 'item',
    '#title' => t('Reference site'),
    '#required' => TRUE,
    '#description' => t('Choose the site to use as reference.'),
    'foo' => array(
      '#type' => 'textfield',
      '#default_value' => '',
    ),
  );

  $form['keep-from'] = array(
    '#title' => t('Use existing screenshots for the base site.'),
    '#type' => 'checkbox',
    '#default_value' => 0,
  );
  $form['keep-to'] = array(
    '#title' => t('Use existing screenshots for the reference site.'),
    '#type' => 'checkbox',
    '#default_value' => 1,
  );

  return $form;
}

/**
 * Implements hook_node_type_view().
 */
function hosting_drulenium_node_view($node) {
  if ($node->type != 'site') {
    return;
  }

  $query = db_select('drulenium_vr_releases', 'vr_re');
  $query->join('hosting_drulenium_vr_releases', 't', 'vr_re.rid = t.rid');
  $result = $query
    ->fields('vr_re', array('rid'))
    ->condition('t.nid', $node->nid)
    ->orderBy('vr_re.rid', 'DESC')
    ->range(0, 2)
    ->execute();

  $release_id = $result->fetchfield();

  if (!empty($release_id)) {
    $line = drulenium_release_admin_link($release_id);

    $older = $result->fetchfield();
    if (!empty($older)) {
      $line .= '<br />' . drulenium_release_compare_link($older, $release_id);
    }
    $query = db_select('drulenium_vr_results', 'r');
    $query
      ->fields('r', array('release_cmp_id'))
      ->condition('r.release_base_id', $release_id)
      ->orderBy('r.rid', 'DESC')
      ->range(0, 1);

    $result = $query
      ->execute();

    $other_release_id = $result->fetchfield();
    if (!empty($other_release_id)) {
      $line .= '<br />' . drulenium_release_compare_link($release_id, $other_release_id, 'Compare last other');
    }

    $node->content['info']['drulenium'] = array(
      '#type' => 'item',
      '#title' => t('Drulenium'),
      '#weight' => 100,
      '#markup' => $line,
    );
  }
}

function drulenium_release_admin_link($release_id) {
  return l(t('Latest snapshot'), 'admin/structure/drulenium/vr/release/' . $release_id);
}

function drulenium_release_compare_link($older_release_id, $release_id, $link_text = 'Compare previous') {
  return l($link_text, 'admin/structure/drulenium/vr/compare/' . $older_release_id . '/' . $release_id);
}

/**
 * Implements hook_form_alter().
 */
function hosting_drulenium_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'site_node_form') {

    $form['hosting_drulenium_site_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Drulenium pages'),
      '#default_value' => isset($form['#node']->hosting_drulenium_site_pages) ? $form['#node']->hosting_drulenium_site_pages : '',
      '#description' => t('Pages the Drulenium screenshot task should visit. Specify pages by using their paths. Enter one path per line. The * character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. Use &lt;front&gt; for home page.'),
    );
    return $form;
  }
}
/**
 * Implements hook_node_insert().
 */
function hosting_drulenium_node_insert($node) {
  return hosting_drulenium_node_update($node);
}

/**
 * Implements hook_node_update().
 */
function hosting_drulenium_node_update($node) {
  if ($node->type == 'site' && isset($node->hosting_drulenium_site_pages)) {
    db_merge('hosting_drulenium_site_pages')
      ->key(array('vid' => $node->vid))
      ->fields(array(
        'nid' => $node->nid,
        'pages' => $node->hosting_drulenium_site_pages,
      ))
      ->execute();
  }
}

/**
 * Implements hook_node_delete().
 */
function hosting_drulenium_pages_node_delete($node) {
  if ($node->type == 'site') {
    hosting_drulenium_pages_delete($node);
  }
}

/**
 * Implements hook_node_load().
 */
function hosting_drulenium_node_load($nodes, $types) {
  if (in_array('site', $types)) {
    foreach ($nodes as $nid => $node) {
      $additions = db_query("SELECT pages AS 'hosting_drulenium_site_pages' FROM {hosting_drulenium_site_pages} WHERE vid = :vid", array(':vid' => $node->vid))->fetchAssoc();
      if (!empty($additions)) {
        foreach ($additions as $key => $value) {
          $nodes[$nid]->$key = $value;
        }
      }
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function hosting_drulenium_node_delete($node) {
  if ($node->type == 'site') {
    db_delete('hosting_drulenium_site_pages')
      ->condition('vid', $node->vid)
      ->execute();
  }
}
