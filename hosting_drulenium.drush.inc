<?php

/**
 * @file
 * Drush code for hosting_drulenium.
 */

/**
 * Implements drush_hook_pre_COMMAND().
 *
 * This runs for each task during the command
 *    drush @hostmaster hosting-tasks
 *
 * NOTE: This ONLY runs when being called from a hostmaster task.
 * This hook should ONLY be used to pass options from a hostmaster task form to
 * the $task object, or if you don't need this functionality from the command
 * line.
 */
function drush_hosting_drulenium_pre_hosting_task($task) {
  drush_log(dt("[Drulenium] in drush_hosting_drulenium_pre_hosting_task"));
  $task = &drush_get_context('HOSTING_TASK');

  if ($task->ref->type == 'site' && $task->task_type == 'dr_run_compare') {
    $task->args[] = '@' . ltrim($task->task_args['reference'], '@');
    $task->options['keep-from'] = $task->task_args['keep-from'];
    $task->options['keep-to'] = $task->task_args['keep-to'];
  }
}
