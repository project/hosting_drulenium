<?php

/**
 * @file
 * Provision/Drush hooks for the hosting_drulenium module.
 *
 * These are the hooks that will be executed by the drush_invoke function.
 */

/**
 * Implements hook_drush_command().
 */
function hosting_drulenium_drush_command() {
  $items['provision-dr_create'] = array(
    'description' => 'Create Drulenium screenshots',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['provision-dr_run_compare'] = array(
    'description' => 'Run compare for Drulenium',
    'options' => array(
      'keep-from' => 'Do not update screenshots for the reference site',
      'keep-to' => 'Do not update screenshots for the site under test',
    ),
    'arguments' => array(
      'from' => 'site alias of the reference site.',
      'to' => 'site alias of the site under test.',
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_init().
 */
function hosting_drulenium_drush_init() {
drush_log(__FILE__.__LINE__ , 'debug');
  hosting_drulenium_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function hosting_drulenium_provision_register_autoload() {
  static $loaded = FALSE;
drush_log(__FILE__.__LINE__ , 'debug');
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Expose the service type this extension defines to provision.
 *
 * Implements hook_provision_services().
 *
 * @return
 *   An array with the service type the key, and the default implementation the value.
 */
function hosting_drulenium_provision_services() {
  hosting_drulenium_provision_register_autoload();
drush_log(__FILE__.__LINE__ , 'debug');
  return array('drulenium' => NULL);
}


/**
 * Implements the provision-drulenium_dr_run_compare command.
 */
function drush_hosting_drulenium_provision_dr_run_compare($alias_from = NULL, $alias_to = NULL) {
  if (d()->name == '@hostmaster') {
    hosting_drulenium_start_compare_task($alias_from, $alias_to);
  }
  else {
    // No OP, this runs in the context of $alias_from. And we want to work in @hostmaster.
    drush_log('Postponing processing until hosting_drulenium_post_hosting_dr_run_compare_task to get into the right context', 'debug');
  }
}

/**
 * Implements the provision-drulenium_dr_run_compare command.
 */
function drush_hosting_drulenium_provision_dr_create() {
  // No OP, this runs in the subject site. And we want to work in @hostmaster.
}

/**
 * Implements hook_post_hosting_TASK_TYPE_task().
 */
function hosting_drulenium_post_hosting_dr_create_task($task) {
  $nid = $task->ref->nid;
  $uri = $task->ref->hosting_name;
  $rid = _drush_dr_create_release_id($nid, $uri);
}

/**
 * Implements hook_post_hosting_TASK_TYPE_task().
 */
function hosting_drulenium_post_hosting_dr_run_compare_task($task) {
  $alias_from = $task->ref->title;
  $alias_to = $task->task_args['reference'];
  $keep_from = $task->task_args['keep-from'];
  $keep_to = $task->task_args['keep-to'];
  hosting_drulenium_start_compare_task($alias_from, $alias_to, $keep_from, $keep_to);
}

/**
 * Helper function to start a comparison.
 *
 * @global type $base_url
 *
 * @param string $alias_from
 *   Drush alias for from site.
 * @param string $alias_to
 *   Drush alias for to site.
 * @param bool $keep_from
 *   Option to keep exsisting screenshots.
 * @param bool $keep_to
 *   Option to keep exsisting screenshots.
 */
function hosting_drulenium_start_compare_task($alias_from = NULL, $alias_to = NULL, $keep_from = FALSE, $keep_to = FALSE) {
  drush_errors_on();
  drush_log("Comparing from:$alias_from - to:$alias_to", 'info');

  // Lookup if a release already exists for the aliases.
  $rid_from = _drush_alias_to_release_id($alias_from, $keep_from);
  $rid_to = _drush_alias_to_release_id($alias_to, $keep_to);

  drush_drulenium_visual_regression_diff($rid_from, $rid_to, $reset = TRUE);

  global $base_url;

  drush_log("View results on $base_url/" . _drulenium_secure_compare_link($rid_from, $rid_to), 'success');

  // Tell hostmaster no changes need to be saved to it's drush context.
  drush_set_option('provision_save_config', FALSE);
}

/**
 * Helper to translate a Drush alias into a node ID.
 *
 * @param string $alias
 *   Drush alias.
 *
 * @return int
 *   Node ID for the site in Aegir.
 */
function _drush_alias_to_site_nid($alias) {

  $alias = ltrim($alias, '@');

  // Lookup the Aegir node ID for the site.
  $nodes = node_load_multiple(NULL, array("title" => $alias, 'type' => 'site'));
  if (!empty($nodes)) {
    $node = current($nodes);
    $nid = $node->nid;
    drush_log(dt('Found @nid as node ID for @name', array('@nid' => $nid, '@name' => $alias)), 'notice');
    return $nid;
  }
}

/**
 * Translate a Drush alias name to a Drulenium relese ID.
 *
 * @param string $alias
 *   The Drush alias.
 *
 * @param bool $keep
 *   Option to keep exsisting screenshots, or create new ones.
 */
function _drush_alias_to_release_id($alias, $keep, $site_nid = NULL) {
  $alias = ltrim($alias, '@');

  if (empty($site_nid)) {
    $site_nid = _drush_alias_to_site_nid($alias);
  }

  if ($site_nid && $keep) {
    $query = db_select('drulenium_vr_releases', 'vr_re');
    $query->leftjoin('hosting_drulenium_vr_releases', 't', 'vr_re.rid = t.rid');
    $query->condition('t.nid', $site_nid)
      ->orderBy('vr_re.rid', 'DESC')
      ->range(0, 1);
    $query->fields('vr_re', array('rid'));
    $release_id = $query->execute()->fetchfield();
  }

  // Either not keeping the old screenshot, or none found.
  if (empty($release_id)) {
    $release_id = _drush_dr_create_release_id($site_nid, $alias);
  }
  else {
    drush_log("Keeping earlier snapshot of $alias", 'ok');
  }
  return $release_id;
}

/**
 * Call Drulenium to create screenshots.
 *
 * @param int $site_nid
 *   Node ID for the site in Aegir.
 * @param string $uri
 *   The URI to use, e.g. "www.example.com"
 *   Optional, when empty it defaults to the hosting_name from the Aegir node.
 *
 * @return int
 *   The Drulenium release_id
 */
function _drush_dr_create_release_id($site_nid, $uri = NULL) {
  $auth = '';
  $scheme = 'http://';
  $pages_cs = '';

  if ($site_nid) {
    $node = node_load($site_nid);
    if (empty($uri)) {
      $uri = $node->hosting_name;
    }

    if (!empty($node->http_basic_auth_username)) {
      $auth = $node->http_basic_auth_username . ':' . $node->http_basic_auth_password . '@';
    }
    if (!empty($node->ssl_enabled) && $node->ssl_enabled > 0) {
      $scheme = 'https://';
    }
    $pages_cs = implode(',', explode("\n", $node->hosting_drulenium_site_pages));
  }
  else {
    $context = d($uri);

    $auth = '';
    if (!empty($context->http_basic_auth_username)) {
      $auth = $context->http_basic_auth_username . ':' . $context->http_basic_auth_password . '@';
    }

    if (!empty($context->ssl_enabled) && $context->ssl_enabled > 0) {
      $scheme = 'https://';
    }

    $uri = $context->uri;

    $pages_cs = implode(',', $context->hosting_drulenium_site_pages);
  }

  $url = $scheme . $auth . $uri . '/';
  $name = $uri . '@' . format_date(REQUEST_TIME, 'short');

  // Do the creation.
  provision_backend_invoke('@hostmaster', 'vr-create',
    array($url, $name),
    array('pages' => $pages_cs)
    );

  // Lookup the new release_id.
  $query = db_select('drulenium_vr_releases', 'vr_re');
  $release_id = $query
    ->fields('vr_re', array('rid'))
    ->condition('vr_re.name', $name)
    ->execute()->fetchfield();

  if (!empty($site_nid)) {
    // Store the relation.
    $record = array('rid' => $release_id, 'nid' => $site_nid);
    drupal_write_record('hosting_drulenium_vr_releases', $record);
  }

  return $release_id;
}
