<?php

/**
 * @file
 * The drulenium service base class.
 */
class Provision_Service_drulenium extends Provision_Service {
  public $service = 'drulenium';

  /**
   * Add the needed properties to the site context.
   */
  static function subscribe_site($context) {
    drush_log(__FILE__.__LINE__ , 'debug');
    $context->setProperty('hosting_drulenium_site_pages');
  }
}

