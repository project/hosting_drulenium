<?php

/**
 * @file
 * Expose the hosting_drulenium feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 *
 * This will be used to generate the 'admin/hosting' page.
 */
function hosting_drulenium_hosting_feature() {
  $features['drulenium'] = array(
    'title' => t('Drulenium integration'),
    'description' => t('Enables Visual Regression testing.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_drulenium',
    'group' => 'experimental',
  );

  return $features;
}
